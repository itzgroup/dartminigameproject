import 'dart:io';

import 'Bullet.dart';
import 'Obj.dart';
import 'Rocket.dart';
import 'Rock.dart';

class TableMap {
  late int width = 10;
  late int height = 5;
  late Rocket rocket;
  late Rock rock;
  late Bullet bullet;
  late TableMap map;
  late int num = 0;
  late int point = 0;
  late List<Obj> listobj = [
    Obj(0, 0, ""),
    Obj(0, 0, ""),
    Obj(0, 0, ""),
    Obj(0, 0, ""),
    Obj(0, 0, ""),
    Obj(0, 0, ""),
    Obj(0, 0, ""),
    Obj(0, 0, ""),
    Obj(0, 0, ""),
    Obj(0, 0, ""),
    Obj(0, 0, ""),
    Obj(0, 0, ""),
    Obj(0, 0, ""),
    Obj(0, 0, ""),
    Obj(0, 0, ""),
    Obj(0, 0, ""),
    Obj(0, 0, ""),
    Obj(0, 0, ""),
    Obj(0, 0, ""),
    Obj(0, 0, "")
  ];
  late int objectcount = 0;

  TableMap();

  void showMap() {
    print(": ♦ : MAP : ♦ :");
    for (int x = 0; x < width; x++) {
      stdout.write("  ");
      for (int y = 0; y < height; y++) {
        stdout.write(" ");
        printSymbolmap(x, y);
      }
      print(" ");
    }
  }

  void printSymbolmap(int x, int y) {
    String symbol = '-';
    for (int i = 0; i < objectcount; i++) {
      if (listobj[i].isOn(x, y)) {
        symbol = listobj[i].getSymbol();
      }
    }
    stdout.write(symbol);
  }

  void checkRock(int x, int y) {
    String symbol = "-";
    bool check = false;
    for (x; x >= 0; x--) {
      if (check == true) {
        check = false;
      } else {
        for (int i = 0; i < objectcount; i++) {
          if (listobj[i].getSymbol() == "o" &&
              listobj[i].isOn(x, rocket.getY())) {
            listobj[i].setSymbol(symbol);
            check = true;
            point++;
          }
        }
      }
    }
  }

  void checkBullet() {
    String symbol = "i";
    for (int i = 0; i < objectcount; i++) {
      if (listobj[i] is Bullet) {
        listobj[i].setSymbol(symbol);
      }

      if (listobj[i].getSymbol() == "i") {
        listobj[i].setX(rocket.getX() - 1);
        listobj[i].setY(rocket.getY());
      }
    }
  }

  void Bulletchange() {
    String symbol = "-";
    for (int i = 0; i < objectcount; i++) {
      if (listobj[i].getSymbol() == "i") {
        listobj[i].setSymbol(symbol);
      }
    }
  }

  int Level2() {
    String symbol = "o";
    for (int i = 0; i <= objectcount; i++) {
      if (listobj[i] is Rock) {
        listobj[i].setSymbol(symbol);
      }

      listobj[0].setX(3);
      listobj[0].setY(1);
      listobj[1].setX(0);
      listobj[1].setY(0);
      listobj[2].setX(1);
      listobj[2].setY(3);
      listobj[3].setX(1);
      listobj[3].setY(4);
      listobj[4].setX(0);
      listobj[4].setY(4);
      listobj[5].setX(2);
      listobj[5].setY(1);
    }
    return 0;
  }

  void addObj(Obj obj) {
    listobj[objectcount] = obj;
    objectcount++;
  }

  void setRocket(Rocket rocket) {
    this.rocket = rocket;
    addObj(rocket);
  }

  void setRock(Rock rock) {
    this.rock = rock;
    addObj(rock);
  }

  void setBullet(Bullet bullet) {
    this.bullet = bullet;
    addObj(bullet);
  }

  int checkWin() {
    if (point == 12) {
      return 1;
    }
    return 0;
  }

  int checkStage2() {
    if (point == 6) {
      return 1;
    } else {
      return 0;
    }
  }

  int checkLose() {
    for (int i = 0; i < objectcount; i++) {
      if (listobj[i].getSymbol() == "o" && listobj[i].isOn(9, 0)) {
        return 1;
      } else if (listobj[i].getSymbol() == "o" && listobj[i].isOn(9, 1)) {
        return 1;
      } else if (listobj[i].getSymbol() == "o" && listobj[i].isOn(9, 2)) {
        return 1;
      } else if (listobj[i].getSymbol() == "o" && listobj[i].isOn(9, 3)) {
        return 1;
      } else if (listobj[i].getSymbol() == "o" && listobj[i].isOn(9, 4)) {
        return 1;
      } else if (listobj[i].getSymbol() == "o" && listobj[i].isOn(9, 5)) {
        return 1;
      } else if (listobj[i].getSymbol() == "o" &&
          listobj[i].isOn(rocket.getX(), rocket.getY())) {
        return 1;
      }
    }
    return 0;
  }

  void showPoint() {
    print("♦ POINT : $point/12 ♦");
  }

  bool inMap(int x, int y) {
    return (x >= 0 && x < width) && (y >= 0 && y < height);
  }

  int getWidth() {
    return width;
  }

  int getHeight() {
    return height;
  }

  Rocket getRocket() {
    return rocket;
  }

  Rock getRock() {
    return rock;
  }

  Bullet getBullet() {
    return bullet;
  }

  bool isOnRock(int x, int y) {
    return rock.isOn(x, y);
  }

  // void showObjectList() {
  //   for (int i = 0; i < objectcount; i++) {
  //     int xja = listobj[i].getX();
  //     int yja = listobj[i].getY();
  //     print("index = $i Symbol " + listobj[i].getSymbol() + " X: $xja Y: $yja");
  //   }
  // }
}
