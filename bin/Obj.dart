class Obj {
  late int x;
  late int y;
  late String symbol;
  late int hp;

  Obj(this.x, this.y, this.symbol);

  int getX() {
    return x;
  }

  void setX(int x) {
    this.x = x;
  }

  void setY(int y) {
    this.y = y;
  }

  int getY() {
    return y;
  }

  int getHp() {
    return hp;
  }

  String getSymbol() {
    return symbol;
  }

  void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }
}
