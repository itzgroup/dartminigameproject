import 'Obj.dart';
import 'TableMap.dart';

class Rocket extends Obj {
  late int x;
  late int y;
  late String symbol;
  late Rocket rocket;
  late TableMap map;
  late String type;

  Rocket(this.x, this.y, this.symbol, this.map) : super(0, 0, "x");

  @override
  int getX() {
    return x;
  }

  @override
  int getY() {
    return y;
  }

  @override
  String getSymbol() {
    return symbol;
  }

  @override
  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }

  bool canFly(int x, int y) {
    return map.inMap(x, y);
  }

  bool fly(String direction) {
    switch (direction) {
      case "a":
        if (flyW()) return false;
        break;
      case "d":
        if (flyE()) return false;
        break;
      default:
        return false;
    }
    return true;
  }

  bool flyW() {
    if (canFly(x, y - 1)) {
      y = y - 1;
    } else {
      return true;
    }
    return false;
  }

  bool flyE() {
    if (canFly(x, y + 1)) {
      y = y + 1;
    } else {
      return true;
    }
    return false;
  }
}
