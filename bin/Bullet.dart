import 'Obj.dart';
import 'TableMap.dart';

class Bullet extends Obj {
  late int x;
  late int y;
  late String symbol;
  late TableMap map;
  late Bullet buu0;

  Bullet(this.x, this.y, this.symbol, this.map) : super(0, 0, "");

  @override
  int getX() {
    return x;
  }

  @override
  int getY() {
    return y;
  }

  @override
  String getSymbol() {
    return symbol;
  }

  @override
  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }
}
