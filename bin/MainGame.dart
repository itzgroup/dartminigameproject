import 'Bullet.dart';
import 'Obj.dart';
import 'Rocket.dart';
import 'TableMap.dart';
import 'Rock.dart';
import 'dart:io';

void main(List<String> arguments) {
  TableMap map = TableMap();
  Rocket rk = Rocket(9, 2, "x", map);
  Rock r = Rock(0, 0, map);
  Rock r2 = Rock(0, 2, map);
  Rock r3 = Rock(0, 4, map);
  Rock r4 = Rock(1, 3, map);
  Rock r5 = Rock(1, 0, map);
  Rock r6 = Rock(2, 1, map);
  List<Rock> rock = [];
  rock.add(r);
  rock.add(r2);
  rock.add(r3);
  rock.add(r4);
  rock.add(r5);
  rock.add(r6);
  int num = 0;
  Bullet buu0 = Bullet(rk.getX() - 1, rk.getY(), "i", map);
  map.setRock(r);
  map.setRock(r2);
  map.setRock(r3);
  map.setRock(r4);
  map.setRock(r5);
  map.setRock(r6);
  map.setRocket(rk);

  showWelcome();
  int ready = int.parse(stdin.readLineSync()!);
  if (ready == 1) {
    while (true) {
      map.showMap();
      map.showPoint();
      showChoice();
      if (map.checkWin() == 1) {
        print("♥ ♦ ♥ ♦ ♥ ♦ ♥ ♦ ♥ ♦ ♥ ♦ ♥");
        print("♥ ♦ ♥ ♦ You Winnn ♥ ♦ ♥ ♦");
        print("♥ ♦ ♥ ♦ ♥ ♦ ♥ ♦ ♥ ♦ ♥ ♦ ♥");
        break;
      }

      if (map.checkLose() == 1) {
        print("☻ ☺ ☻ ☺ ☻ ☺ ☻ ☺ ☻ ☺ ☻ ☺ ☻");
        print("☺ ☻ ☺ ☻ You Lose  ☺ ☻ ☺ ☻");
        print("☻ ☺ ☻ ☺ ☻ ☺ ☻ ☺ ☻ ☺ ☻ ☺ ☻");
        break;
      }

      String direction = stdin.readLineSync()!;
      if (direction == "q") {
        break;
      } else if (direction == 'a') {
        rk.fly(direction);
        for (int i = 0; i < rock.length; i++) {
          rock[i].dorp();
        }
        map.Bulletchange();
      } else if (direction == 'd') {
        rk.fly(direction);
        for (int i = 0; i < rock.length; i++) {
          rock[i].dorp();
        }
        map.Bulletchange();
      } else if (direction == 'w') {
        num++;
        if (num == 1) {
          map.setBullet(buu0);
        }
        map.checkBullet();
        map.checkRock(rk.getX(), rk.getY());
        // map.showObjectList();
        if (map.checkStage2() == 1) {
          map.Level2();
        }
      }
    }
  } else if (ready == 2) {
    print("Bye bye.");
  }
}

void showWelcome() {
  print("♥ ♦ ♥ ♦ ♥ ♦ ♥ ♦ ♥ ♦ ♥ ♦ ♥ ♦ ♥ ♦ ♥ ♦ ♥");
  print(":♦:♦: Welcome to Rocket Pew Pew :♦:♦:");
  print(":♦:♦: You are Ready? :♦:♦:");
  print(":♦:♦: Press 1 to in game :♦:♦:");
  print(":♦:♦: Press 2 to Exit game :♦:♦:");
  print("♥ ♦ ♥ ♦ ♥ ♦ ♥ ♦ ♥ ♦ ♥ ♦ ♥ ♦ ♥ ♦ ♥ ♦ ♥");
}

void showChoice() {
  print("♦: Choice Menu :♦");
  print("Press a fly to the left :");
  print("Press d fly to the right :");
  print("Press w for Shoot! :");
  print("Press q to Exit Game :");
}
