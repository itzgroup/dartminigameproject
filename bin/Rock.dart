import 'Obj.dart';
import 'TableMap.dart';

class Rock extends Obj {
  late int x;
  late int y;
  late TableMap map;

  Rock(this.x, this.y, this.map) : super(0, 0, "o");

  @override
  int getX() {
    return x;
  }

  @override
  int getY() {
    return y;
  }

  @override
  String getSymbol() {
    return symbol;
  }

  @override
  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }

  bool canDrop(int x, int y) {
    return map.inMap(x, y);
  }

  bool dorp() {
    if (canDrop(x, y)) {
      x = x + 1;
    } else {
      return true;
    }
    return false;
  }
}
